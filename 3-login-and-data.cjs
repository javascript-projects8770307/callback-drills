/*
NOTE: Do not change the name of this file

NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs/promises.

Q1. Create 2 files simultaneously (without chaining).
Wait for 2 seconds and starts deleting them one after another. (in order)
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)


Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file
Delete the original file 
Using promise chaining
*/


function login(user, val) {
    if (val % 2 === 0) {
        return Promise.resolve(user);
    } else {
        return Promise.reject(new Error("User not found"));
    }
}

function getData() {
    return Promise.resolve([
        {
            id: 1,
            name: "Test",
        },
        {
            id: 2,
            name: "Test 2",
        }
    ]);
}

function logData(user, activity) {
    // use promises and fs to save activity in some file
}

/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
    
*/

const fs = require('fs');

// Q1. Create 2 files simultaneously (without chaining).
// Wait for 2 seconds and starts deleting them one after another. (in order)

function deleteFiles(fileNames) {
    setTimeout(() => {
        files = fileNames.split(',');
        let allFiles = files.map(file => {
            fs.unlink(file, (err) => {
                if (err) {
                    console.error(err);
                } else {
                    console.log(file, " Deleted successfully")
                }
            })
            return file;
        })
        console.log("Files to be Deleted", allFiles);
    }, 2 * 1000)
}
function createFirstFile() {
    return new Promise((resolve, reject) => {
        fs.writeFile('1.json', '', (err) => {
            if (err) {
                reject(new Error(err));
            } else {
                resolve("File 1 Created successfully")

            }
        })
    })
}
function createSecondFile() {
    return new Promise((resolve, reject) => {
        fs.writeFile('2.json', '', (err) => {
            if (err) {
                reject(new Error(err));
            } else {
                resolve('File 2 created Successfully');
            }
        })
    })
}

createFirstFile().then(message=>{
    console.log(message);
}).catch(message=>{
    console.log(message)
})
createSecondFile().then(message=>{
    console.log(message)
    deleteFiles('1.json,2.json')
}).catch(message=>{
    console.log(message)
})

// Q2. Create a new file with lipsum data (you can google and get this). 
// Do File Read and write data to another file
// Delete the original file 
// Using promise chaining


const lipsumData = "lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

function createFile(fileName, data) {
    return new Promise((resolve, reject) => {
        fs.writeFile(fileName, data, (err) => {
            if (err) {
                reject(new Error("Something went wrong: " + err));
            } else {
                resolve("File Created Successfully");
            }
        })
    })
}

function readFile(filePath) {
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, (err, data) => {
            if (err) {
                reject(new Error("Something went wrong: " + err));
            } else {
                resolve(data.toString());
            }
        })
    })
}

function deleteFile(fileName) {
    return new Promise((resolve, reject) => {
        fs.unlink(fileName, (err) => {
            if (err) {
                reject(new Error("Something went wrong: " + err));
            } else {
                resolve("File deleted Successfully");
            }
        })
    })
}

createFile('lipsum.txt', lipsumData).then(message => {
    console.log(message);
    return readFile('lipsum.txt');
}).then(content => {
    return createFile('newLipsum.txt', content);
}).then(message => {
    console.log(message);
    return deleteFile('lipsum.txt');
}).then(message => {
    console.log(message);
}).catch(message => {
    console.log(message);
})






/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
    
*/

function login(user, val) {
    if (val % 2 === 0) {
        return Promise.resolve(user);
    } else {
        return Promise.reject(new Error("User not found"));
    }
}

function getData() {
    return Promise.resolve([
        {
            id: 1,
            name: "Test",
        },
        {
            id: 2,
            name: "Test 2",
        }
    ]);
}

function logData(user, activity) {

}


login('madan', 2).then(user => {
    console.log("Login Success",);
    fs.appendFile('LoginActivity.json', '{ user : ' + user + 'Activity : Login Success' + 'TimeStamp: ' + new Date() + '}\n', (err) => {
        if (err) {
            console.log(err);
        } else {
            console.log("Activity Saved")
        }
    })
    return getData();

}).then(message => {
    console.log("Get Data Success", message);

}).catch(message => {
    console.log(message)
    fs.appendFile('LoginActivity.json', '{ user : ' + user + 'Activity : Login Failure' + 'TimeStamp: ' + new Date() + '}\n', (err) => {
        if (err) {
            console.log(err);
        } else {
            console.log("Activity Saved")
        }
    })
})
