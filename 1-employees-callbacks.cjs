/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file

*/

const fs = require('fs');

function getDataforId(callback) {
    fs.readFile('data.json', (err, data) => {
        if (err) {
            console.log(err);
        } else {
            let employeeData = JSON.parse(data);
            let dataForId = employeeData['employees'].filter(employee => {
                if (employee.id === 2 || employee.id === 13 || employee.id === 23) {
                    return employee;
                }
            })
            callback(null, dataForId)

        }
    })

}

function companiesGroup(callback) {
    fs.readFile('data.json', (err, data) => {
        if (err) {
            console.log(err);
        } else {
            let employeeData = JSON.parse(data);
            let companyGroup = employeeData['employees'].reduce((groups, employee) => {
                let company = employee.company;
                if (!groups[company]) {
                    groups[company] = [];
                } else {
                    groups[company].push(employee);
                }
                return groups;
            }, {})

            callback(null, companyGroup);
        }
    })
}

function removeEntry(callback) {
    fs.readFile('data.json', (err, data) => {
        if (err) {
            console.log(err);
        } else {
            let employees = JSON.parse(data);
            let removed = employees['employees'].reduce((groups, employee) => {
                let id = employee.id;
                if (!groups['employees']) {
                    groups['employees'] = []
                } else if (id !== 2) {
                    groups['employees'].push(employee);
                }
                return groups;
            }, {})
            callback(null, removed);
        }
    })
}
function sortData(callback) {
    fs.readFile('data.json', (err, data) => {
        let employeeData = JSON.parse(data);
        let sorted = employeeData['employees'].sort((first, second) => {
            if (first.company === second.company) {
                return first.id - second.id;
            }
            return ('' + first.company).localeCompare(second.company);
        })
        callback(null, sorted);
    })
}

function swapPosition(callback) {
    fs.readFile('data.json', (err, data) => {
        data = JSON.parse(data);
        let indexes = data['employees'].reduce((group, employee) => {
            if (employee.id === 93 || employee.id === 92) {
                group.push(data['employees'].indexOf(employee));
            }
            return group;
        }, [])
        tempCompany = data['employees'][indexes[1]].company;
        data['employees'][indexes[1]].company = data['employees'][indexes[0]].company;
        data['employees'][indexes[0]].company = tempCompany;

        callback(null, data['employees']);

    })

}

// For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

function addBirthDay(callback) {
    fs.readFile('data.json', (err, data) => {
        data = JSON.parse(data);
        let birthdayData = data['employees'].map(employee => {
            if(employee.id %2 == 0){
                employee.BirthDay = new Date();
            }
            return employee;
        })
        callback(null, birthdayData);

    })
}

getDataforId((err, data) => {
    if (err) {
        console.log(err);
    } else {
        data = JSON.stringify(data);
        fs.writeFile('output1.json', data, (err) => {
            if (err) {
                console.log(err);
            } else {
                console.log('output1.json Created Successfully');

                companiesGroup((err, data) => {
                    if (err) {
                        console.log(err);
                    } else {
                        data = JSON.stringify(data);
                        fs.writeFile('output2.json', data, (err) => {
                            if (err) {
                                console.log(err);
                            } else {
                                console.log("Data Saved to output2.json successfully");
                                fs.readFile('output2.json', (err, data) => {
                                    if (err) {
                                        console.log(err);
                                    } else {
                                        data = JSON.parse(data)['Powerpuff Brigade'];
                                        data = JSON.stringify(data);
                                        fs.writeFile('output3.json', data, (err, data) => {
                                            if (err) {
                                                console.log(err);
                                            } else {
                                                console.log("Data saved to output3.json file successfully")
                                                removeEntry((err, data) => {
                                                    if (err) {
                                                        console.log(err);
                                                    } else {
                                                        data = JSON.stringify(data);
                                                        fs.writeFile('output4.json', data, (err) => {
                                                            if (err) {
                                                                console.log(err);
                                                            } else {
                                                                console.log("Removed data saved to output4.json file successfully")
                                                                sortData((err, data) => {
                                                                    if (err) {
                                                                        console.log(err);
                                                                    } else {
                                                                        data = JSON.stringify(data);
                                                                        fs.writeFile('output5.json', data, (err) => {
                                                                            if (err) {
                                                                                console.log(err);
                                                                            } else {
                                                                                console.log("Sorted Data saved in output5.json successfully");
                                                                                swapPosition((err, data) => {
                                                                                    if (err) {
                                                                                        console.log(err);
                                                                                    } else {
                                                                                        data = JSON.stringify(data);
                                                                                        fs.writeFile('output6.json', data, (err) => {
                                                                                            if (err) {
                                                                                                console.log(err);
                                                                                            } else {
                                                                                                console.log('Data swapped and saved in output6.json file');
                                                                                                addBirthDay((err, data) => {
                                                                                                    if(err){
                                                                                                        console.log(err);
                                                                                                    }else{
                                                                                                        data = JSON.stringify(data);
                                                                                                        fs.writeFile('output7.json',data,(err)=>{
                                                                                                            if(err){
                                                                                                                console.log(err);
                                                                                                            }else{
                                                                                                                console.log("Birthday Added Successfully saved in output7.json file")
                                                                                                            }
                                                                                                        })
                                                                                                    }
                                                                                                })


                                                                                            }
                                                                                        })
                                                                                    }
                                                                                })
                                                                            }
                                                                        })
                                                                    }
                                                                })
                                                            }
                                                        })
                                                    }
                                                })
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    }
});





