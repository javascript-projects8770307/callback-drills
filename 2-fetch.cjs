
/*
NOTE: Do not change the name of this file

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.

Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

Using promises and the `fetch` library, do the following. 

1. Fetch all the users
2. Fetch all the todos
3. Use the promise chain and fetch the users first and then the todos.
4. Use the promise chain and fetch the users first and then all the details for each user.
5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

NOTE: If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.

Usage of the path libary is recommended

*/

const path = require('path');
const fs = require('fs');

// 1. Fetch all the users
fetch("https://jsonplaceholder.typicode.com/users")
    .then((response) => {
        if (response.ok) {
            return response.json();
        }
    })
    .then((data) => {

        const Filepath = path.resolve("allUsers.json");
        data = JSON.stringify(data);

        fs.writeFile(Filepath, data, (err) => {
            if (err) {
                console.error(err);
            } else {
                console.log("allUser File saved successfully");
            }
        })
    })
    .catch(message => {
        console.log("Something went wrong", message);
    })

// 2. Fetch all the todos
fetch("https://jsonplaceholder.typicode.com/todos")
    .then((response) => {
        if (response.ok) {
            return response.json();
        }
    })
    .then((data) => {

        const Filepath = path.resolve("ToDolist.json");
        data = JSON.stringify(data);

        fs.writeFile(Filepath, data, (err) => {
            if (err) {
                console.error(err);
            } else {
                console.log("ToDoList File saved successfully");
            }
        })
    })
    .catch(message => {
        console.log(message);
    })



// 3. Use the promise chain and fetch the users first and then the todos.
function fetchUser() {
    return new Promise((resolve, reject) => {
        fetch("https://jsonplaceholder.typicode.com/users")
            .then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    reject(new Error("Data can't be fetched"));
                }
            })
            .then((data) => {
                resolve(data);
            })
            .catch(message => {
                reject(new Error(message));
            })
    })
}

function fetchToDo() {
    return new Promise((resolve, reject) => {
        fetch("https://jsonplaceholder.typicode.com/todos")
            .then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    reject(new Error("Todo Data can't be fetched"));
                }
            })
            .then((data) => {
                resolve(data);
            })
            .catch(message => {
                reject(new Error(message))
            })
    })
}

fetchUser().then(data => {
    console.log("All users", data)
    return fetchToDo();
})
    .then(data => {
        console.log("All Todos", data);
    })
    .catch(err => {
        console.log(err);
    })


// 4. Use the promise chain and fetch the users first and then all the details for each user.